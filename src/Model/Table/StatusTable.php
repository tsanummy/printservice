<?php
namespace App\Model\Table;

use App\Model\Entity\Status;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Status Model
 */
class StatusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('status');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('active', 'valid', ['rule' => 'boolean'])
            ->allowEmpty('active');

        return $validator;
    }
    public function reserve(){
        $query = $this->query();
        $query->insert(['active'])
            ->values([
                'active' => '1'
            ])
            ->execute();
        $id = $this
                ->find()
                ->select(['id'])
                ->first();
        return $id["id"];
    }       

    public function free($id){
        return $this
                ->query()
                ->delete()
                ->where(['id' => $id])
                ->execute();
    }
    public function isBusy(){
        $result = $this
                ->find()
                ->first();
        if (is_null($result)){
            return false;
        }
        else {
            return true;
        }
    }
    
}
