<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Printjob Entity.
 */
class Printjob extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user' => true,
        'copies' => true,
        'date' => true,
    ];
}
