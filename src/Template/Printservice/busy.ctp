<h1>Estado de la impresora</h1>

<p>La impresora esta OCUPADA. Por favor espere a que este libre.</p>

<p>El estado actual de la impresora es: </p>

    <div id="content">  
        <b><i><?php echo $printerstatus; 
        ?></i></b>
    </div> 

<br>

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
(function($)
{
    $(document).ready(function()
    {
        $.ajaxSetup(
        {
            cache: true,
        });
        var $container = $("#content");
        $container.load("/print/busy");
        var refreshId = setInterval(function()
        {
            $container.load('/print/busy');
        }, 100000);
    });
})(jQuery);
</script>
