<p><a href="/users/logout"><i class="fa fa-sign-out"></i> Terminar sesion</a></p>
<h1><i class="fa fa-print"></i> Imprimir proyecto</h1>
<div>
    <em><p>Para imprimir su proyecto cargue un fichero PDF (No se admiten otros formatos).<br>
    Dispone unicamente de UNA (1) copia. Revise que el fichero elegido es el correcto.</p></em>
</div>  
<br>
<h3>Cargar PDF</h3>
<?php echo $this->Form->create('pdfadd1', array('type' => 'file', 'required' => true));?>
    <fieldset>
    <?php
        echo $this->Form->file('submittedfile');
        echo 'Collate? ', $this->Form->checkbox('collate');
        echo '<br># of Copies ',$this->Form->number('copies',array('value' => 1));
    ?>
    </fieldset>
<?= $this->Form->button('Cargar fichero') ?>
<?= $this->Form->end() ?>
