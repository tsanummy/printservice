<h1>Estado de la impresora</h1>

<p>Su documento se esta imprimiendo ahora.</p>

<p>El estado de la impresora es:</p>

    <div id="content">  
       <b><i> <?php echo $printerstatus; 
        ?></i></b>
    </div> 

<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
(function($)
{
    $(document).ready(function()
    {
        $.ajaxSetup(
        {
            cache: true,
        });
        var $container = $("#content");
        $container.load("/print/status");
        var refreshId = setInterval(function()
        {
            $container.load('/print/status');
        }, 100000);
    });
})(jQuery);
</script>
