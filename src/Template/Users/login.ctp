<h1><i class="fa fa-lock"></i> Inicio de Sesion</h1>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<?= $this->Form->create() ?>
<?= $this->Form->label('username', "<i class='fa fa-user'></i> Usuario", array('escape' => false)) ?>
<?= $this->Form->input("username", array('label' => false, 'required' => true))?>
<?= $this->Form->label('password', "<i class='fa fa-key'></i> Clave", array('escape' => false)) ?>
<?= $this->Form->password("password", array('label' => false, 'required' => true, 'type' => 'password'))?>
<?= $this->Form->button('Login') ?>
<?= $this->Form->end() ?>
