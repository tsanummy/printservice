<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Printjob'), ['action' => 'edit', $printjob->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Printjob'), ['action' => 'delete', $printjob->id], ['confirm' => __('Are you sure you want to delete # {0}?', $printjob->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Printjobs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Printjob'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="printjobs view large-10 medium-9 columns">
    <h2><?= h($printjob->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('User') ?></h6>
            <p><?= h($printjob->user) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($printjob->id) ?></p>
            <h6 class="subheader"><?= __('Copies') ?></h6>
            <p><?= $this->Number->format($printjob->copies) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Date') ?></h6>
            <p><?= h($printjob->date) ?></p>
        </div>
    </div>
</div>
