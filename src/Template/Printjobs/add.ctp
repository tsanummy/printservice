<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('List Printjobs'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="printjobs form large-10 medium-9 columns">
    <?= $this->Form->create($printjob); ?>
    <fieldset>
        <legend><?= __('Add Printjob') ?></legend>
        <?php
            echo $this->Form->input('user');
            echo $this->Form->input('copies');
            echo $this->Form->input('date', array('empty' => true, 'default' => ''));
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
