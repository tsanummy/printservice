<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Printjob'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="printjobs index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('user') ?></th>
            <th><?= $this->Paginator->sort('copies') ?></th>
            <th><?= $this->Paginator->sort('date') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($printjobs as $printjob): ?>
        <tr>
            <td><?= $this->Number->format($printjob->id) ?></td>
            <td><?= h($printjob->user) ?></td>
            <td><?= $this->Number->format($printjob->copies) ?></td>
            <td><?= h($printjob->date) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $printjob->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $printjob->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $printjob->id], ['confirm' => __('Are you sure you want to delete # {0}?', $printjob->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
