<p><a href="/users/logout"><i class="fa fa-sign-out"></i> Terminar sesion</a></p>

<h1>Panel de administracion</h1>

<h3><a href="/print"><i class="fa fa-print"></i> Para imprimir pincha aqu&iacute;</a></h3>
<br>
<p><i class="fa fa-database"></i> Para las herramientas de administrador selecciona de la lista siguiente:</p>

<ul>
    <li><a href="/subjects"><i class="fa fa-book"></i> Administracion de Asignaturas</a></li>
    <li><a href="/years"><i class="fa fa-calendar"></i> Administracion de Cursos academicos</a></li>
    <li><a href="/printjobs"><i class="fa fa-line-chart"></i> Log de impresiones</a></li>
    <li><a href="/status"><i class="fa fa-exclamation-triangle"></i> Control de estados de impresora</a>&nbsp;<em>(Para limpiar estados fantasma en caso de ca&iacute;da del sistema)</em></li>
</ul>


