<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Core\Configure;
/**
 * Admin Controller
 *
 */
class AdminController extends AppController
{
    
    public function beforeFilter(\Cake\Event\Event $event){
        if ($this->request->session()->read('User.role') != 'admin'){
            return $this->redirect('/users/login');
        }
    }
    public function admin(){
    }
}
