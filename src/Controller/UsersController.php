<?php
namespace App\Controller;

use Cake\Core\Configure;

use App\Controller\AppController;

/**
 * Users Controller
 *
 */
class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\Event $event){
        $this->Auth->allow('add');
    }
    public function error($errorcode){
        switch ($errorcode){
            case 0:
                $errormessage = "Datos de inicio de sesion incorrectos.";
                $this->Flash->error($errormessage);
                break;
            case 1: 
                $errormessage = "No se pudo contactar con la base de datos.";
                $this->Flash->error($errormessage);
                break;
            case 2:
                $errormessage = "Usted no esta matriculado en el proyecto, en caso de que esto sea un error contacte con el webmaster de la FIC (webmaster.fic@udc.es)";
                $this->Flash->error($errormessage);
                break;
            case 3:
                $errormessage = "Su proyecto no se encuentra en estado depositado o en defensa, en caso de que esto sea un error contacte con el webmaster de la FIC (webmaster.fic@udc.es)";
                $this->Flash->error($errormessage);
                break;
            case 4:
                $errormessage = "Usted no esta matriculado en este curso academico, en caso de que esto sea un error contacte con el webmaster de la FIC (webmaster.fic@udc.es)";
                $this->Flash->error($errormessage);
                break;
            case 5:
                $errormessage = "Su proyecto no se encuentra ni en estado depositado ni en defensa, en caso de que esto sea un error contacte con el webmaster de la FIC (webmaster.fic@udc.es)";
                $this->Flash->error($errormessage);
                break;
            default:
                $this->redirect('/');
        }

    }
    public function login(){
        if ($this->request->is('post')) {
            if ($this->Auth->user('id')){
                $this->redirect('/print');
            }
            else{
                $ldapdata = Configure::read('ldap');
                $mysqldata = Configure::read('mysql');
                $adminldapuser = $ldapdata['ldapuser'];
                $adminldappassword = $ldapdata['ldappass'];
                $ldaphost = $ldapdata['ldaphost'];
                $mysqluser = $mysqldata['mysqluser'];
                $mysqlpassword = $mysqldata['mysqlpass'];
                $mysqlhost = $mysqldata['mysqlhost'];
                $querypro = "SELECT webfic.UserProfile.loginName
                            FROM webfic.Instancia, webfic.UserProfile
                            WHERE webfic.Instancia.idUserProfile = webfic.UserProfile.usrId
                            and (estado=\"Depositado\" or estado = \"En Defensa\")
                            and webfic.UserProfile.loginName=";

                $baseDN = "dc=cc,dc=fic,dc=udc,dc=es";
                $requser = $this->request->data['username'];
                $user = "uid=".$requser.",ou=people,ou=docencia,dc=cc,dc=fic,dc=udc,dc=es";
                $password = $this->request->data['password'];

                $ldapconn = ldap_connect($ldaphost)
                    or die("Could not connect to LDAP server.");
                ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
                ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
                if ($ldapconn){
                    $ldapbind = ldap_bind($ldapconn, $adminldapuser, $adminldappassword);
                    if($ldapbind){
                        $authenticate = ldap_bind($ldapconn,$user,$password);
                        if($authenticate){
                            $filter = "(uid=".$requser.")";
                            if (!($search = ldap_search($ldapconn, $baseDN, $filter))){
                                die("Unable to search ldap server");
                            }
                            $info = ldap_get_entries($ldapconn, $search);
                            $this->loadModel('Subjects');  
                            $this->loadModel('Years');
                            $activeYears = $this->Years->get_active_years();
                            $resultArray = $this->Subjects->get_subjects();
                            $enrolled = false;
                            $matriculated = false;
                            $admin = false;
                            for ($i=0; $i<$info[0]['employeetype']['count']; $i++){
                                foreach ($resultArray as $subject){
                                    if(strpos($info[0]['employeetype'][$i], $subject->code) !== false){
                                        $enrolled = true;
                                    }
                                }
                                foreach ($activeYears as $year){
                                    if(strpos($info[0]['employeetype'][$i], 'curso.'.$year->name) !== false){
                                        $matriculated = true;
                                    }
                                }
                                if($info[0]['employeetype'][$i] == 'wikific.cecafi'){       
                                    $admin = true;
                                    $this->request->session()->write('User.role', 'admin');
                                } else{
                                    $this->request->session()->write('User.role', 'user');
                                }
                                 
                            }
                            if ($admin || $requser == "m.castillo"){
                                $this->Auth->setUser(array($user));
                                $this->request->session()->write('User.id', $requser);
                                return $this->redirect($this->Auth->redirectUrl());
                            } else {
                                if($enrolled){
                                    if($matriculated){
                                        $dbhandle = mysql_connect($mysqlhost, $mysqluser, $mysqlpassword) 
                                            or die("MySQL error.");
                                        $selected = mysql_select_db("webfic",$dbhandle) 
                                            or die("MySQL error.");
                                        $querypro .= "\"".mysql_real_escape_string($requser)."\";";
                                        $result = mysql_query($querypro);
                                        mysql_close($dbhandle);
                                        if (mysql_num_rows($result) == 1){
                                            $this->Auth->setUser(array($user));
                                            $this->request->session()->write('User.id', $requser);
                                            return $this->redirect($this->Auth->redirectUrl());
                                        } else{
                                            $this->error(5);
                                        }
                                    } else{
                                        $this->error(4);
                                    }
                                } else{
                                    $this->error(2);
                                }
                            }
                        } else{
                            //Usuario pass incorrectos
                            $this->error(0);
                        }
                    } else{
                        //No se puede hacer el bind con el usuario administrador
                        $this->error(1);
                    }
                }else{  
                        $this->error(1);
                }

            }
        } 
    }

    public function logout(){
        $this->Flash->success('You are now logged out.');
        $this->Auth->setUser(false);
        $this->request->session()->write('User.role', 'anonymous');
        $this->request->session()->write('User.id', '');
        return $this->redirect($this->Auth->logout());
    }
}
