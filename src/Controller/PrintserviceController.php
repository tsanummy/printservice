<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Core\Configure;
/**
 * Printservice Controller
 *
 */
class PrintserviceController extends AppController
{
    
    public function beforeFilter(\Cake\Event\Event $event){
        if ($this->request->session()->read('User.role') != 'admin' && $this->request->session()->read('User.role') != 'user'){
            return $this->redirect('/users/login');
        }
    }
    function checkstatus(){
        $url = 'http://10.10.7.100/status.htm';
        $toparse = file_get_contents($url);
        if (strpos($toparse,'Ready To Print')){ 
            return true;
        }
        else{ 
            return false;      
        }
    }     
    public function status(){
        if ($this->checkstatus()){
            $printerstatus = 'Impresion finalizada / Impresora Libre';
        } else{
            $printerstatus = 'Impresion en curso / Impresora Ocupada';
        }
        $this->set('printerstatus', $printerstatus);
    }
    public function busy(){
        if ($this->checkstatus()){
            $printerstatus = 'Impresora Libre';
        } else{
            $printerstatus = 'Impresion en curso / Impresora Ocupada';
        }
        $this->set('printerstatus', $printerstatus);
    }
    public function formprint(){

        if ($this->request->is('post')) {
            $uid = $this->request->session()->read('User.id');
            $file = $_FILES['submittedfile']['tmp_name'];
            $extension = $_FILES['submittedfile']['type'];
            $printservice = Configure::read('printparams');
            $multicopy = $printservice['multicopy'];
            if ($multicopy){
                $collate = 'false';
                if ($this->request->data['collate'] == 1){
                    $collate = 'true';
                } else{
                    $collate = 'false';
                }
                $copies = $this->request->data['copies'];
            }
            $this->loadModel('Status');
            $this->loadModel('Printjobs');
            if ($this->Printjobs->canprint($uid)){
                if (!$this->Status->isBusy()){
                    $id = $this->Status->reserve();
                    if ($this->checkstatus()){ 
                        if ($extension == 'application/pdf'){
                            $command = escapeshellcmd('python scriptimprimir.py '.$file.' 
                                                      '.$copies.' '.$collate);
                            $output = shell_exec($command);
                            $this->Printjobs->addprintjob($uid, $copies);
                            sleep(1);
                            $this->Status->free($id);
                            $this->setAction('status');
                        }
                    }
                    else{
                        $this->Status->free($id);   
                        $this->setAction('busy');
                    }
                }
                else{
                    $this->setAction('busy');
                }
            } else{
                $this->Flash->error('No se puede imprimir: Numero maximo de copias alcanzado (1 copia).');
            }       
        } else{
            $printservice = Configure::read('printparams');
            $multicopy = $printservice['multicopy'];
            $this->autoRender = false;
            if ($multicopy){
                $this->render('formprintcopies');
            } else {
                $this->render('formprint');
            }
        }
    }  
}
