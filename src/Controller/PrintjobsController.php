<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Printjobs Controller
 *
 * @property \App\Model\Table\PrintjobsTable $Printjobs
 */
class PrintjobsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    
    public function beforeFilter(\Cake\Event\Event $event){
        if ($this->request->session()->read('User.role') != 'admin' && $this->request->session()->read('User.role') != 'user'){
            return $this->redirect('/users/login');
        }
    }

    public function index()
    {
        $this->set('printjobs', $this->paginate($this->Printjobs));
        $this->set('_serialize', ['printjobs']);
    }

    /**
     * View method
     *
     * @param string|null $id Printjob id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $printjob = $this->Printjobs->get($id, [
            'contain' => []
        ]);
        $this->set('printjob', $printjob);
        $this->set('_serialize', ['printjob']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $printjob = $this->Printjobs->newEntity();
        if ($this->request->is('post')) {
            $printjob = $this->Printjobs->patchEntity($printjob, $this->request->data);
            if ($this->Printjobs->save($printjob)) {
                $this->Flash->success('The printjob has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The printjob could not be saved. Please, try again.');
            }
        }
        $this->set(compact('printjob'));
        $this->set('_serialize', ['printjob']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Printjob id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $printjob = $this->Printjobs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $printjob = $this->Printjobs->patchEntity($printjob, $this->request->data);
            if ($this->Printjobs->save($printjob)) {
                $this->Flash->success('The printjob has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The printjob could not be saved. Please, try again.');
            }
        }
        $this->set(compact('printjob'));
        $this->set('_serialize', ['printjob']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Printjob id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $printjob = $this->Printjobs->get($id);
        if ($this->Printjobs->delete($printjob)) {
            $this->Flash->success('The printjob has been deleted.');
        } else {
            $this->Flash->error('The printjob could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
